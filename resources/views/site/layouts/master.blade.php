<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>JM-Drive</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{ url('css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="{{ url('css/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <main>

    <nav class="light-blue lighten-1" role="navigation">
      <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">JM-Drive</a>
        <ul class="right hide-on-med-and-down">
          <li><a href="#">Navbar Link</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
          <li><a href="#">Navbar Link</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
      </div>
    </nav>

    @yield('content')

  </main>

  <footer class="page-footer blue">
    <div class="container">
      <div class="row">
        <div class="col s12">
          <h5 class="white-text">JM-Drive</h5>
          <p class="grey-text text-lighten-4">JM-Drive is an open file management system, inspired by Google Drive, builded with Laravel and created with tons of love!</p>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container right-align">
      Made with <i class="material-icons tiny">favorite</i> by <span class="orange-text text-lighten-3" >PaulVL</span>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="{{ url('js/materialize.js') }}"></script>
  <script src="{{ url('js/init.js') }}"></script>
  <script src="{{ url('js/pvl-icons.js') }}"></script>
  @yield('js')

  </body>
</html>
