@extends('site.layouts.master')

@section('content')

  <div class="section no-pad-bot">

  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <div class="col s12">

          <table class="bordered highlight">
            <thead>
              <tr menu-data="11">
                  <th data-field="id">Nombre</th>
                  <th data-field="name">Creado por</th>
                  <th data-field="price">Modificado</th>
                  <th data-field="price">Tamaño</th>
              </tr>
            </thead>

            <tbody>
              @foreach($files as $file)
              <tr class="option-element" element-type="file" element-attr='{"file-id": {{ $file->id }}}'>
                <td class="valign-wrapper pvl-icon" pvl-icon="{{ $file->icon() }}">{{ $file->name }}</td>
                <td>Eclair</td>
                <td>{{ $file->updated_at }}</td>
                <td></td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>

        <div class="fixed-action-btn" style="bottom: 200px; right: 24px;">
          <a class="btn-floating btn-large red">
            <i class="large material-icons">add</i>
          </a>
          <ul>
            <li class="tooltipped" data-position="left" data-delay="50" data-tooltip="Nueva carpeta..." ><a class="btn-floating transparent modal-trigger" href="javascript:void(0)" data-target="#add-folder-modal"><img src="{{ url('images/icons/all/add_folder.png') }}" alt="" class="circle responsive-img"></a></li>
            <li class="tooltipped" data-position="left" data-delay="50" data-tooltip="Subir archivos..."><a class="btn-floating transparent"><img src="{{ url('images/icons/all/upload.png') }}" alt="" class="circle responsive-img"></a></li>
        </div>

        <div id="add-folder-modal" class="modal">
          <div class="modal-content">
            <h4>Modal Header</h4>
            <p>A bunch of text</p>
          </div>
          <div class="modal-footer">
            <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
          </div>
        </div>
            
        </div>
      </div>

    </div>
            


    <br><br>
  </div>
  </div>

@stop

@section('js')
  <script src="{{ url('js/options-menu.js') }}"></script>
  
  <script>
    $(document).ready(function(){
      // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
      $('.modal-trigger').leanModal();
    });
  </script>
@stop