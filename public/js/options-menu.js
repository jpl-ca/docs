$(document).ready(function(){

	optionsMenu = document.createElement('ul');
	optionsMenu.setAttribute("class", "z-depth-1");
	optionsMenu.setAttribute("id", "options-menu");
	$('main').append(optionsMenu);

	function createMenuElement(icon, text, classes, attributes)
	{
		classes = typeof classes !== 'undefined' ? classes : null;

		attributes = typeof attributes !== 'undefined' ? attributes : null;

		attrClass = "options-menu-element valign-wrapper pvl-icon";

		if(classes != null)
		{
			$.each(classes, function(index, value){
				attrClass += ' ' + value;
			});
		}

		menuElement = document.createElement('li');
		menuElement.setAttribute("class", attrClass);
		menuElement.setAttribute("pvl-icon", icon);

		if(attributes != null)
		{
			attributes = jQuery.parseJSON(attributes);

			$.each(attributes, function(key, value){
				menuElement.setAttribute(key, value);
			});

		}

		elementText = document.createTextNode(text);
		menuElement.appendChild(elementText);
		$("#options-menu").append(menuElement);
		pvlIcon(menuElement);
	}

	function emptyOptionsMenu()
	{
	 	$("#options-menu").empty();
	}

	function anywhereMenu(attributes)
	{
		createMenuElement('add_folder', 'Nueva carpeta...', ['action-add-folder'], attributes);
	}

	function folderMenu(attributes)
	{
		createMenuElement('opened_folder', 'Abrir carpeta...', ['action-open-folder'], attributes);
		createMenuElement('add_folder', 'Nueva carpeta...', ['action-add-folder'], attributes);
	}

	function fileMenu(attributes)
	{
		createMenuElement('download', 'Descargar', ['action-download-file'], attributes);
		createMenuElement('folder', 'Mover a...', ['action-move-to'], attributes);
	}

	function createMenu(type, attributes)
	{
		attributes = typeof attributes !== 'undefined' ? attributes : null;

		emptyOptionsMenu();

		switch(type){
			case 'folder':
				folderMenu(attributes);
				break;
			case 'file':
				fileMenu(attributes);
				break;
			case 'anywhere':
		        anywhereMenu(attributes);
		        break;
		    default:
		        anywhereMenu(attributes);
		        break;
		}
	}

	$(document).on("contextmenu",function(e){
      if(e.target.class != "INPUT" && e.target.nodeName != "TEXTAREA")
      {
        e.preventDefault();
      }
    });

    $(document).mousedown(function(event) {
    	clickedElement = event.target;
      	closestTr = $(clickedElement).closest( "tr" );

      if( $(closestTr).hasClass("option-element") )
      {
      	console.log('se hizo click en una fila');
      	elementType = closestTr.attr('element-type');
      	elementAttributes = closestTr.attr('element-attr');

        switch(event.which){
          case 3:
            createMenu(elementType, elementAttributes);
            displayOptionsMenu(event);
            break;
          case 1:
            $("#options-menu").hide(100);
            break;
        }
      }else if( $(clickedElement).hasClass("options-menu-element") ){
      	console.log('se hizo click en un elemento del menu');
        switch(event.which){
          case 1:
          	doMenuAction(clickedElement);
            $("#options-menu").hide(100);
            break;
        }
      }
      else{  //clicked any other place
      	console.log('se hizo click en un espacio el blanco');
        switch(event.which){
          case 3:
            createMenu('anywhere');
            displayOptionsMenu(event);
            break;
          case 1:          
          	//doMenuAction(clickedElement);
            $("#options-menu").hide(100);
            break;
        }
      }
    });

    function displayOptionsMenu(event)
    {
      $("#options-menu").hide(100);
      $("#options-menu").finish().toggle(100).css({
          top: event.pageY + "px",
          left: event.pageX + "px"
      });
    }

    function doMenuAction(element)
    {
    	console.log('se disparo accion del menu');

    	if( $(element).hasClass('action-add-folder') ){
	  		//alert( "add folder triggered" );
	  		$('#add-folder-modal').openModal();
    	} else if( $(element).hasClass('action-download-file') ){
    		fileId = $(element).attr('file-id');
	  		alert('descargar archivo con id: ' +  fileId);
    	}
    }

});
