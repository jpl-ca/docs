$( ".pvl-icon" ).each(function( element ) {

	var icon = $(this).attr('pvl-icon');

	var iconUrl = 'images/icons/all/' + icon + '.png';

	var iconImg = document.createElement('img');
  	iconImg.setAttribute("src", iconUrl);

	$( this ).prepend( iconImg );

	$( this ).removeClass( "pvl-icon" );
});

function pvlIcon(element)
{
	var icon = $(element).attr('pvl-icon');

	var iconUrl = 'images/icons/all/' + icon + '.png';

	var iconImg = document.createElement('img');
  	iconImg.setAttribute("src", iconUrl);

	$( element ).prepend( iconImg );

	$( element ).removeClass( "pvl-icon" );
}