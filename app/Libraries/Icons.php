<?php

namespace App\Libraries;

class Icons {
	private static $unsupportedImageExtensions = ['unsupported' => ['jpeg'], 'default' => 'image_file'];

	public static function getIcon($extension)
	{
		if(in_array($extension, self::$unsupportedImageExtensions['unsupported']))
		{
			return self::$unsupportedImageExtensions['default'];
		}else{
			return 'file';
		}
	}
}