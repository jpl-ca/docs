<?php

namespace App\Libraries;

use Storage;
use File;
use Carbon\Carbon;
use App\File as FileModel;
use App\FileVersion as FileVersionModel;

class Cloudy {

    const STORAGE_FOLDER = 'files';
    const VERSION_CONTROL_FOLDER = 'versioning';
    protected $cachedFile = null;

	public function __call($name, $arguments)
    {
        if($name === 'download')
        {
        	$name = isset($arguments[0]) ? $arguments[0] : null;
            return $this->downloadFile($name);
        }else{
            $trace = debug_backtrace();
            trigger_error('Method ' . $name . ' must have an array as argument, on line ' . $trace[0]['line'], E_USER_NOTICE);
            return null;
        }
    }

    public static function __callStatic($name, $arguments)
    {
		if($name === 'download'){
            $name = $arguments[0];
            $name = isset($arguments[0]) ? $arguments[0] : null;
            return call_user_func(array(new self, 'downloadFile'), $name);
        }else{
            $trace = debug_backtrace();
            trigger_error('Method ' . $name . ' must have an array as argument, on line ' . $trace[0]['line'], E_USER_NOTICE);
            return null;
        }
    }

    public function put($file, $directory_id = null, $versioning = true){

        $extension = $file->getClientOriginalExtension();
        $filename = $this->createName() . '.' . $extension;
        $originalFilename = str_replace('.' . $extension, '', $file->getClientOriginalName());

        $fileRecord = FileModel::firstOrNew([
            'directory_id' => $directory_id,
            'name' => $originalFilename,
            'extension' => $extension
        ]);

        $oldstoragePath = self::STORAGE_FOLDER . '/' . $fileRecord->filename;

        $fileRecord->filename = $filename;
        $fileRecord->versioning = $versioning;

        $storagePath = self::STORAGE_FOLDER . '/' . $filename;

        if($fileRecord->exists)
        {
            Storage::delete($oldstoragePath);
        }

        Storage::put($storagePath, File::get($file));

        $fileRecord->save();

        if( $fileRecord->versioning )
        {
            $versionControlPath = self::VERSION_CONTROL_FOLDER . '/' . $filename;

            Storage::copy($storagePath, $versionControlPath);

            $version = $fileRecord->currentVersionNumber() + 1;

            $fileVersionRecord = FileVersionModel::create([
                'file_id' => $fileRecord->id,
                'filename' => $filename,
                'version' => $version
            ]);
        }

        $this->cachedFile = $filename;

    	return $this;
    }

    public static function delete(FileModel $file)
    {

    }

    private function downloadFile($name = null, $versioned = false)
    {
    	if(!is_null($this->cachedFile))
    	{
    		$name = $this->cachedFile;
    		$this->cachedFile = null;
    	}

        if($versioned)
        {
            $path = 'app/' . self::VERSION_CONTROL_FOLDER . '/';
        }else{
            $path = 'app/' . self::STORAGE_FOLDER . '/';
        }

    	$file = storage_path($path . $name);
        return response()->download($file);
    }

    private function createName()
    {
    	return str_random(5) . Carbon::now()->format('U') . str_random(5) ;
    }

    private function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
	
}