<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\File;

class FileVersion extends Model
{
    protected $fillable = ['file_id', 'filename', 'version'];
    
    public function file()
    {
    	return $this->belongTo(File::class, 'file_id', 'id');
    }
}
