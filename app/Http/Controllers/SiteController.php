<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\File as FileModel;

class SiteController extends Controller
{
    public function getIndex()
    {
        return view('site.index');
    }

    public function getDashboard()
    {
    	$files = FileModel::all();
        return view('site.dashboard', compact('files'));
    }
}
