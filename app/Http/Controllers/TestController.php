<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Storage;
use File;

use App\Libraries\Cloudy;

class TestController extends Controller
{
    public function getCase0()
    {
        $c = new Cloudy;
        return dd($c->createName());
        return Cloudy::download('asd');
    }

    public function getCase1()
    {
        Storage::disk('local')->put('file.txt', 'Contents');
        return 'file created';
    }

    public function getCase2()
    {
        $files = Storage::files('files');
        return dd($files);
    }

    public function getCase3()
    {
        return view('test.case3');
    }

    public function postCase3(Request $request)
    {

        $file = $request->file('filefield');

        $cloudy = new Cloudy;
        $cloudy->put($file);//->download();

        return 'file saved!';

        exit();
    /*    $entry = new Fileentry();
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $file->getFilename().'.'.$extension;
 
        $entry->save();*/

        $f = storage_path('app/drive/'.$filename);
        $headers = array(
              'Content-Type: application/pdf',
            );
        return response()->download($f, 'filename.pdf', $headers);

        return 'file saved!';
    }

    public function getCase4(Request $request)
    {

        $f = storage_path('app/drive/phppfKkfo.png');
        return response()->download($f);

        return 'file saved!';
    }
}
