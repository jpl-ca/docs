<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FileVersion;

use App\Libraries\Icons;

class File extends Model
{
    protected $fillable = ['directory_id', 'name', 'extension', 'filename', 'versioning'];

    protected $casts = [
        'versioning' => 'boolean',
    ];

    public function currentVersionNumber()
    {
    	$lastFileVersion = $this->fileVersions()->orderBy('version', 'desc')->first();
    	$currentVersionNumber = (!is_null($lastFileVersion)) ? $lastFileVersion->version : 0;
    	return $currentVersionNumber;
    }

    public function fileVersions()
    {
    	return $this->hasMany(FileVersion::class, 'file_id', 'id');
    }

    public function icon()
    {
        return Icons::getIcon($this->extension);
    }
}
